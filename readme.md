![histogram_and_fit](https://gitlab.com/filipchudzynski/stock-market-non-gaussianity-analyzer/-/raw/master/plots/hist_fit_Bitcoin_year1.png)
This is a repository with library I wrote for my master thesis, which was used to produce results published in [Acta Physica Polonica](http://przyrbwn.icm.edu.pl/APP/PDF/139/app139z4p14.pdf)

Introduction:

Criticality and long-range correlation tend to manifest themselves via scale invariance observed in, e.g., different systems such as human heart rate. It was suggested that critical phenomena or, in other words, the instability of the system in finances can be linked to market crashes. Indeed, it was demonstrated that 2 min S&P 500, which exhibits scaling law in variance λ^2, entered a critical state manifested with data collapse during the market crash in 1987. To investigate the link between criticality and market crash more closely we decided to examine the Bitcoin, which is known for its huge price swings and speculative character. We were able to show scale invariance in the range of hours persisting from 2012 up to 2019, which suggests that Bitcoin might be in a critical state not only during the crash.

Results and example of how to use the library:

[notebook](https://colab.research.google.com/drive/17zrYcO3D2dLVOGafc7g18IVz1B23X9zU?usp=sharing)
