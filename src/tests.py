from TimeSeries import Timeseries
from DataLoader import BitcoinDataLoader,SandPDataLoader
from datetime import datetime
from Config import Config

def loading_timeseries(dataset):
    if dataset == "SandP":
        X = Timeseries()
        X.load_data( SandPDataLoader("/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/datasets/"))
        X.plot()
        return X
    elif dataset == "Bitcoin":
        X = Timeseries()
        X.load_data( BitcoinDataLoader("/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/datasets/"))
        X.plot()
        return X


def division_into_batches(X:Timeseries,n_of_batches:int):
    X.divide_into_batches(n_of_batches)
    batches_object = X.timeseries_in_batches()
    batches_object.plot_all_series()


def test_suite_loading_timeseries():
    #checks whether data is properly loaded and plotted
    loading_timeseries("SandP")
    loading_timeseries("Bitcoin")

def test_suite_division_into_batches():
    X=Timeseries()
    X.load_data( SandPDataLoader("/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/datasets/"))
    print("One batch")
    division_into_batches(X,1)
    print("Two batches")
    division_into_batches(X,2)
    print("Five batches")
    division_into_batches(X,5)
    print("Twenty batches")
    division_into_batches(X,20)

    Y=Timeseries()
    Y.load_data( BitcoinDataLoader("/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/datasets/"))
    print("One batch")
    division_into_batches(Y,1)
    print("Two batches")
    division_into_batches(Y,2)
    print("Five batches")
    division_into_batches(Y,5)
    print("Twenty batches")
    division_into_batches(Y,20)


def test_loading_cached_data():
    Y=Timeseries()
    filename = "/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/test_"
    suffix=".npy"
    filename_dict = {"histogram":filename+"hist"+suffix,
                    "bins":filename+"bins"+suffix,
                    "fit":filename+"fit"+suffix,
                    "params":filename+"lambdas"+suffix}
    Y.load(filename_dict,
            legacy=False)
    Y.plot_histograms()
    Y.plot_fits()
    params=Y.timeseries_in_batches()._series_in_batches_object[0].get_fits_in_scales().get_params()
    for p in params:
        print(p,params[p][0])



def test_config():
    params = {
        "calculation_type":"",
        "scales_range":{"start":8,"end":2**13,"N":20},
        "dataset":"bitcoin",
        "n_of_batches":64,
        "batch_range":(30,31),
        "bin_calc_method":"modified",
        "dataset_folder":"datasets/",
        "cached_data_folder":"cached_numpy_data_for_plots/recent",
        "fontsize":15}
    config = Config(params=params)
    filenames=config.generate_filenames()
    print(filenames)
    for name in filenames:
        print(name)
    assert filenames["bins"] == "cached_numpy_data_for_plots/recent/bins_bitcoin_scales_8-8192_pts_20_batches_64_batch_range_(30, 31)_bins_calc_modified.npy"

def test_loading_cached_with_config():
    X=Timeseries()
    params = {
        "calculation_type":"Paper_Calculation",
        "scales_range":{"start":8,"end":2**13,"N":20},
        "dataset":"SandP",
        "n_of_batches":16,
        "batch_range":None,
        "bin_calc_method":"optimized",
        "dataset_folder":"datasets/",
        "dataset_filename":'SandP500_2010_11_14__2019_12_31.csv',
        "cached_data_folder":"cached_numpy_data_for_plots/SandP",
        "fontsize":15}
    config = Config(params=params,timeseries=X)
    config.load_timeseries()
    config.divide_into_batches()
    # # X.plot_batches()
    config.load_cached_data(legacy=False)
    
    # time, series = config.get_timeseries().
    X.plot_fits_single_batch(confidence_bands=True)
    X.plot_lambdas(cumulative=True,scatter=False)

def test_fitting():
    X=Timeseries()
    params = {
        "calculation_type":"Paper_Calculation",
        "scales_range":{"start":8,"end":2**13,"N":20},
        "dataset":"SandP",
        "n_of_batches":64,
        "batch_range":None,
        "bin_calc_method":"optimized",
        "fillna":0,
        "dataset_folder":"datasets/",
        "dataset_filename":'SandP500_2010_11_14__2019_12_31.csv',
        "cached_data_folder":"cached_numpy_data_for_plots/SandP",
        "fontsize":15}
    config = Config(params=params,timeseries=X)
    config.load_timeseries()
    config.divide_into_batches()
    config.detrend_in_windows()
    config.calculate_histogram()
    #config.get_timeseries().plot_histograms()
    config.fit(confidence_interval=False)
    Y=Timeseries()
    config = Config(params=params,timeseries=Y)
    config.load_timeseries()
    config.divide_into_batches()
    # # X.plot_batches()
    config.load_cached_data(legacy=False)
    # time, series = config.get_timeseries().
    X.plot_fits_single_batch()


# test_config()
# test_suite_loading_timeseries()
# test_suite_division_into_batches()
# test_loading_cached_data()
test_fitting()
# test_loading_cached_with_config()
