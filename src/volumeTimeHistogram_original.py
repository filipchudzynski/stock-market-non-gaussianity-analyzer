import numpy as np
import pandas as pd
import datetime as dtime
import matplotlib.pyplot as plt

plt.rcParams['xtick.labelsize'] = 30 
plt.rcParams['ytick.labelsize'] = 30 

def generate_nhour_time_bins_in_a_day():
    hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
    interval_lengths = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
    
    hour_intervals = {}
    hist = {}
    for length in interval_lengths:
        hour_intervals[length] = []
        hist[length] = {}
    for length in interval_lengths:
                for begin_index in range(len(hours) - length + 1):
                        hour_intervals[length].append( (begin_index,begin_index+length) )
                        hist[length][begin_index] = 0

    return hour_intervals, hist


def sum_in_interval(start,end):
    sum = 0

    return sum
datasets_folder = "datasets/"
data = pd.read_csv(datasets_folder+'bitstampUSD_1-min_data_2012-01-01_to_2019-08-12.csv',sep=',', index_col='Timestamp').fillna(0)
data["dates_conv"] = pd.to_datetime(data.index,unit='s')
print(data.columns)
dates = data["dates_conv"].values

#for bitcoin
def one_day_intervals_in_timerange(start,end):
                    #get hour           #get minutes
    start = start-np.timedelta64(7,"h")-np.timedelta64(52,"m")

    #panda range of days in data
    days = pd.interval_range(pd.Timestamp(start), pd.Timestamp(end), freq="D")

    return days


def get_tuple_from_interval(intervals,index):
    return intervals.values.to_tuples()[index]

def get_dataframe_from_interval(data,intervals,index):
    return data[(data["dates_conv"] > get_tuple_from_interval(intervals,index)[0]) & (data["dates_conv"] < get_tuple_from_interval(intervals,index)[1]) ]    

def update_histogram_in_hour_bins(hour_intervals,hist, day, data,column):
    

    for interval_length in hour_intervals:
            # print("_______________________________________")
            # print(interval_length)
            for interval_index, interval in enumerate(hour_intervals[interval_length]):
                # print(day + np.timedelta64(interval[0],"h"),"-",day + np.timedelta64(interval[1],"h")) 
                data_range = data[(data["dates_conv"] > day + np.timedelta64(interval[0],"h") ) & \
                    ( data["dates_conv"] < day + np.timedelta64(interval[1],"h"))] 
                #convert to numpy array
                data_range = data_range[column].values
                # print(interval_length, hist[interval_length][interval_index])
                hist[interval_length][interval_index] += np.sum(data_range)
                #TODO iterate data_range in order to sum volume in intervals and add them to respective time intervals
    
    return hist

def calculate_volume_in_hour_bins(start,end,data):
    intervals = one_day_intervals_in_timerange(start,end)
    days = [day_tuples[0] for day_tuples in intervals.values.to_tuples()]
    hour_intervals, hist = generate_nhour_time_bins_in_a_day()
    print(hist)
    for day in days[:3]:
        hist = update_histogram_in_hour_bins(hour_intervals, hist, day, data,"Volume_(Currency)")
    print(hist)
    # np.save("volume_histogram",hist)
    return hist

start, end = dates[0],dates[-1]
intervals = one_day_intervals_in_timerange(start,end)
print(get_dataframe_from_interval(data,intervals,10))
day_tuple = get_tuple_from_interval(intervals,10)
print(data["Close"][ (data["dates_conv"]>day_tuple[0]) & (data["dates_conv"]<day_tuple[1])].values[0])
hist = calculate_volume_in_hour_bins(start,end,data)
# calculate_volume_in_hour_bins(start,end)
# hist = np.load("volume_histogram.npy",allow_pickle=True)
# hist = hist.item()
max_vals = {}
for key in hist:
    print(hist[24][0])
    print([str(starts)+"-"+str(starts+key) for starts in hist[key].keys()])
    max_vals[key] = np.max(np.fromiter(hist[key].values(),dtype=float)/hist[24][0])
    # plt.scatter([str(starts)+"-"+str(starts+key) for starts in hist[key].keys()],np.fromiter(hist[key].values(),dtype=float)/hist[24][0] )
    # plt.title("volume in " + str(key) + " hour bins")
    # plt.get_current_fig_manager().full_screen_toggle()
    # plt.show()
plt.scatter(max_vals.keys(),max_vals.values())
plt.xticks([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24])
plt.title("Maximal amount of transaction for a given interval",FontSize=36)
plt.xlabel("intervals length[hours]",FontSize=36)
plt.ylabel("vol traded in interval[%]",FontSize=36)
plt.show()
# histogram_of_volume_in_time_bins = {}
# #init histogram
# for length in hour_intervals:
#     histogram_of_volume_in_time_bins[length] = {}
#     for tuple_id, _ in hour_intervals[length]:
#         histogram_of_volume_in_time_bins[length][tuple_id]=0


# #loop over day in days
# for day in days:
#     #loop over hour intervals
#     for length in hour_intervals:
        #summing volumes in respective intervals
        # for tuple_id, start_end in hour_intervals[length]:
        #     histogram_of_volume_in_time_bins[length][tuple_id] += sum_in_interval(start_end[0],start_end[1])