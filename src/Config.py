from TimeSeries import Timeseries
from DataLoader import BitcoinDataLoader, SandPDataLoader, SanPCurrentDataLoader, GeneratedDataLoader
class Config:
        """
        wrapper class, which applies configuration to the timeseries class
        """
        def __init__(self,timeseries:Timeseries=None,params:dict=None,
                        calculation_type:str=None,scales_range:tuple=None,
                        dataset:str=None,n_of_batches:int=None,batch_range:tuple=None,
                        bin_calc_method:str=None,dataset_folder:str=None,
                        dataset_filename:str=None, cached_data_folder:str=None,
                        fillna=None,fontsize:int=None, filename:str=None):
                """
                Args:
                {scales_range} = ({from}.{to},{number of points})
                {dataset} = {bitcoin,SandP}
                {n_of_batches} = e.g 13 
                {batch_range} = (10:13)
                {bin_calc_method} = my initial, sturge, scott
                """
                if params != None:
                        self._params = params
                else:
                        params = {"calculation_type":calculation_type,
                                "scales_range":scales_range,
                                "dataset":dataset,
                                "fillna":fillna,
                                "n_of_batches":n_of_batches,
                                "batch_range":batch_range,
                                "bin_calc_method":bin_calc_method,
                                "dataset_folder":dataset_folder,
                                "dataset_filename":dataset_filename,
                                "cached_data_folder":cached_data_folder,
                                "fontsize":fontsize}
                        self._params = params

                self._timeseries = timeseries
                if filename:
                        self._filenames = filename
                else:
                        self._filenames = self.generate_filenames()
                

        def generate_filenames(self):
                """
                Function used to define naming convention for files storing
                data in form of numpy arrays. Filename is build with following template:
                
                {cached_data_folder}/{type_of_data}_{dataset}_{scales_range_begin}_{scales_range_end}_
                {scales_range_n_of_pts}_{n_of_batches}_{batch_range}
                {bin_calc_method}
                
                """
                # function returns name following convention:
                # {type_of_data}_{scales_range}_{dataset}_{n_of_batches}_{bin_calc_method}
                types_of_data = ["bins","histogram","params","fit","report"]
                filenames = []
                for data in types_of_data:
                        filenames.append(
                        ("{}/{}{}_fillna_{}_{}_scales_{}-{}_pts_{}"+
                        "_batches_{}_batch_range_{}_bins_calc_{}.npy").format(
                        self._params["cached_data_folder"],
                        data,
                        self._params["calculation_type"],
                        self._params["dataset"],
                        self._params["fillna"],
                        self._params["scales_range"]["start"],
                        self._params["scales_range"]["end"],
                        self._params["scales_range"]["N"],
                        self._params["n_of_batches"],
                        self._params["batch_range"],
                        self._params["bin_calc_method"]))


                #bin,histogram,fit,params 
                return dict(zip(types_of_data,filenames))

        def load_timeseries(self):
                try:
                        if self._params['dataset'] == "bitcoin":
                                loader = BitcoinDataLoader(self._params['dataset_folder'],
                                                        self._params['dataset_filename'],
                                                        volume=True,fillna=self._params['fillna'])
                        elif self._params['dataset'] == "SandP_legacy":
                                loader = SandPDataLoader(self._params['dataset_folder'],
                                                        self._params['dataset_filename'])
                        elif self._params['dataset'] == "SandP":
                                loader = SanPCurrentDataLoader(self._params['dataset_folder'],
                                                        self._params['dataset_filename'])
                        elif self._params['dataset'] == "Cascade":
                                loader = GeneratedDataLoader(self._params['dataset_folder'],
                                                        self._params['dataset_filename'])
                        else:
                                raise

                        self._timeseries.load_data(loader)
                except IOError as exc:
                        print("no dataset provided",exc)

        def divide_into_batches(self):
                self._timeseries.divide_into_batches(self._params["n_of_batches"],
                                                    self._params["batch_range"])

        def detrend_in_windows(self):
                self._timeseries.detrended_log_returns(self._params["scales_range"])

        def calculate_histogram(self):
                filenames = self._filenames
                self._timeseries.calculate_histogram(filenames["bins"],filenames["histogram"],self._params["bin_calc_method"],False,False)

        def fit(self,confidence_interval=False):
                filenames = self._filenames
                self._timeseries.fit_castaing_equation(filenames["params"],filenames["fit"],filenames["report"],True,confidence_interval)

        def load_cached_data(self,legacy:bool=False):
                
                filenames = self.generate_filenames()
                if legacy == True:
                        print(filenames)
                        params = input("Input the filename of params/filename for given config\n")
                        self._timeseries.load_cached_data(filenames,legacy,params_alternative_filename=params)
                else:
                        self._timeseries.load_cached_data(filenames,legacy)
                
        def get_timeseries(self):
                return self._timeseries
