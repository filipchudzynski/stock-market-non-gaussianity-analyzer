from abc import ABCMeta, abstractmethod
import pandas as pd
import numpy as np

class DataLoader(metaclass = ABCMeta):
    @abstractmethod
    def __init__(self,dataset_folder,dataset_name):
        pass
    #load method should be instantiated with only one design principle in mind:
    #it should return numpy arrays in order and name:
    #time array, data array, timeseries name
    @abstractmethod
    def load(self):
        pass

class BitcoinDataLoader(DataLoader):
    def __init__(self,dataset_folder="datasets/",
                         dataset_name='bitstampUSD_1-min_data_2012-01-01_to_2019-08-12.csv',
                         volume=False,fillna=0):
        self._dataset_folder = dataset_folder
        self._dataset_name = dataset_name
        self._load_volume=volume
        self._fillna=fillna

    def load(self):
        if self._fillna ==0:
            data = pd.read_csv(self._dataset_folder+self._dataset_name,
                        sep=',', index_col='Timestamp').fillna(self._fillna)
        else:
            data = pd.read_csv(self._dataset_folder+self._dataset_name,
                        sep=',', index_col='Timestamp').fillna(method=self._fillna)

        data["dates_conv"] = pd.to_datetime(data.index,unit='s')
        length=data.shape[0]
        # print(len(v))
        series = data["Close"].to_numpy()
        time = data["dates_conv"].to_numpy()
        volume = None
        if self._load_volume == True:
            volume = data["Volume_(Currency)"].to_numpy()
            if self._fillna == 0:
                volume = volume[series != 0]
        
        if self._fillna == 0:
            return time[series != 0],series[series != 0], 'Bitcoin 2012-2019', volume
        else:
            return time, series, 'Bitcoin 2012-2019', volume

    #this method fills Nan's with last non-Nan value


class SanPCurrentDataLoader(DataLoader):
    def __init__(self,dataset_folder="datasets/",
                         dataset_name='SandP500_2010_11_14__2019_12_31.csv',
                         volume=False):
        self._dataset_folder = dataset_folder
        self._dataset_name = dataset_name
        self._load_volume=volume

    def load(self):
        data = pd.read_csv(self._dataset_folder+self._dataset_name)

        # print(len(v))
        series = data["Close"].to_numpy()
        time = data["DateTime"].to_numpy()
        volume = None
        return time,series, 'SandP 2012-2019', volume

class GeneratedDataLoader(DataLoader):
    def __init__(self,dataset_folder="datasets/",
                         dataset_name='generatedCascade.csv'):
        self._dataset_folder = dataset_folder
        self._dataset_name = dataset_name
    
    def load(self):
        data = pd.read_csv(self._dataset_folder + self._dataset_name)
        series = data.values.flatten()
        time = np.arange(len(series))
        #because volume is not available for this data it is set to None
        volume = None
        return time, series, 'Cascade' ,volume



class SandPDataLoader(DataLoader):
    def __init__(self,dataset_folder="datasets/",
                         dataset_name='SandP_log2min.dat'):
        self._dataset_folder = dataset_folder
        self._dataset_name = dataset_name

    def generate_time_axis(self,size):
        # 60 min in hour  
        # *6.5 hour per day(from 1985) https://www.marketwatch.com/story/a-brief-history-of-trading-hours-on-wall-street-2015-05-29
        # *(120)253 working days https://en.wikipedia.org/wiki/Trading_day
        # *amount of lost data(otherwise data doesn't fit to paper) 0.5
        # here I assume that, we don't have all the minute data, that's why
        # x axis doesn't fit
        workingMinutesPerYearAfter = 60 * 6.5 * 253 * 0.49
        workingMinutesPerYearBefore = 60 * 6 * 253 * 0.49
        startDateBefore = 1984
        startDateAfter = 1985
        #compute time range for the plot
        #index 4500 is approximately equal to 1985 in paper
        #based on the plot
        yearsTillEightyFive = np.arange(45000) / workingMinutesPerYearBefore \
                                + startDateBefore

        time = np.arange(size-45000) / workingMinutesPerYearAfter + startDateAfter
        time = np.concatenate((yearsTillEightyFive,time),axis=None)

        return time

    def load(self):
        tempF = open(self._dataset_folder+self._dataset_name,'r')
        #cast strings to float
        series = np.array([float(v) for v in tempF.readlines()])
        # This data is weird, the shape of the curve seems
        # correct, but the values are unreasonable.
        # Values between 9.75 - 11.25
        # The name of the file implies that it is logarithm
        # of base 2 of the data from S&P, but comparing it
        # with an article doesn't give correct values.
        # Calculating 2 to the power of v gives
        # Values between 750 - 2500 vs 150 - 500(from article)
        # Since the guessings gave inconsistent results I solve
        # x*y**(v) where for v I've put upper and lower bounds
        # from plot from article x*y**9.75 = 150, x*y**10.75=450
        # y = 2.647 x = 0.012. Since it's a rough estimation
        # I guess y is actually equal to euler e, and x factor
        # is 0.01, which can point to the fact the data was
        # presented in cents instead of dollars.

        time = self.generate_time_axis(len(series))
        # 0.01 - turn cents into dolar, e**v turn log(cents) into cents
        
        #because volume is not available for this data it is set to None
        volume = None
        return time, 0.01*np.e**series, 'SandP',volume
