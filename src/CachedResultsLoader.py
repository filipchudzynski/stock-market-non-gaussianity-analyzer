import numpy as np

def legacy_load_lambdas(filename=None,lambdas_dict=None,legacy_txt_filename:str=None):
        params = []
        import ast
        print(legacy_txt_filename)
        file = open(legacy_txt_filename,'r')
        contents = file.read()
        dictionary = ast.literal_eval(contents)
        file.close()


        batch_len = max([tuples[0] for tuples in list(dictionary.keys())])
        scaleArray = list(set([tuples[1] for tuples in list(dictionary.keys())]))
        for b in range(batch_len+1):
            params_in_batch={}
            for s in scaleArray:
                params_in_batch[s] = dictionary[(b,s)][:2]        
            params.append(params_in_batch)
        if filename != None:
            np.save(filename,params)
        return params
        # for ind,(k,v) in enumerate(dictionary):
        #         # if k == 7:
        #         #         print(ind)
        #         lambdas[k]={}
        # for k,v in dictionary:
        #         lambdas[k][v] = dictionary[(k,v)]
        # return lambdas

#  def change_dict_into_list_of_dictionaries(self,fit_dict,params_dict):
#         params = []

#         for b in range(len(batches)):
#             params_in_batch={}
#             for s in scaleArray:
#                 params_in_batch[s] = params_dict[(b,s)]        
#             fits.append(fits_in_batch)
#             params.append(params_in_batch)
#         return fits,params
