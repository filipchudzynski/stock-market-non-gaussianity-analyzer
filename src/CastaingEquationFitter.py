from lmfit import Model
from scipy.integrate import quad
import numpy as np
class Fit:
    def __init__(self, A, lambdas, fitted_curve, arguments=None):
        self._A = A
        self._lambdas = lambdas
        self._fitted_curve = fitted_curve
        self._arguments = arguments

class FitInScales:
    def __init__(self, params:dict, fitted_curve:dict,
                    arguments:dict=None, fit_report:dict=None):
        self._params = params
        self._fit_report = fit_report
        self._fitted_curve = fitted_curve
        self._arguments = arguments

    def get_params(self):
        return self._params

    def get_fit(self):
        return self._arguments, self._fitted_curve


class CastaingEquationFitter:
    def integrand(sigma,x,A,lambdas):
        return A/(lambdas*np.sqrt(2*np.pi))*np.exp(-(np.log(sigma)+lambdas**2)**2/(2*lambdas**2))*1/(sigma*np.sqrt(2*np.pi)) * np.exp(-1/2*(x/(sigma))**2)*1/sigma
    # def integrand(sigma,x,lambdas):
    #     return 1/(lambdas*np.sqrt(2*np.pi))*np.exp(-(np.log(sigma)+lambdas**2)**2/(2*lambdas**2))*1/(sigma*np.sqrt(2*np.pi)) * np.exp(-1/2*(x/(sigma))**2)*1/sigma
    # #turn dlnalpha into dalpha
    # def fbis(sigma,x,A,lambdas):
    #     return A/(lambdas*np.sqrt(2*np.pi))*np.exp(-np.log(sigma)**2/(2*lambdas**2))*1/(sigma*np.sqrt(2*np.pi)) * np.exp(-(x**2)/(2*sigma**2)) * 1/sigma

    def CastaingEquation(x,A,lambdas):
        result = []
        args = [()]
        for tempX in x:
            r,e = quad(CastaingEquationFitter.integrand,0,np.Inf,args=(tempX,A,lambdas))
            result.append(r)
        return np.array(result)

    def fit_data(Y,X):
            multiplicativeModel = Model(CastaingEquationFitter.CastaingEquation)
            multiplicativeModel.set_param_hint('lambdas', min=0,max=2)
            fitResult = multiplicativeModel.fit(Y,x=X,A=500,lambdas=0.4)
            
            # print(fitResult.fit_report())
            # print(fitResult.covar)
            return fitResult.best_values['lambdas'], fitResult.best_values['A'],\
                    fitResult.fit_report(), fitResult.best_fit

    def fit_data_with_conf_interval(Y,X):
            multiplicativeModel = Model(CastaingEquationFitter.CastaingEquation)
            multiplicativeModel.set_param_hint('lambdas', min=0,max=2)
            fitResult = multiplicativeModel.fit(Y,x=X,A=500,lambdas=0.4)
            print(fitResult.fit_report())
            print(fitResult.covar)
            conf_bands_delim = fitResult.eval_uncertainty(sigma=3)
            return fitResult.best_values['lambdas'], fitResult.best_values['A'],\
                    fitResult.fit_report(), fitResult.best_fit, conf_bands_delim

