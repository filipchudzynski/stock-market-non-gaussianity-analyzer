import numpy as np
from abc import ABCMeta, abstractmethod

class Histogram(metaclass = ABCMeta):
    def __init__(self,bins,histogram_points):
        self._hist=histogram_points
        self._bins=bins

    def calculate_number_of_bins(method,length,index,std,bin_length_index=0,density=10):
        if method == "optimized":
                print( "optimized")
                return int(12*np.sqrt(length))
        if method == "modified":
                print( "modified")
                return int(length/(index+1))
        if method == "len":
                print("len")
                return length
        if method == "sturge":
                print("sturge")
                return int(1+3.222* np.log(length))
        if method == "scott":
                print("scott")
                return int(3.49 * std * length**(-1.0/3.0))
        if method == "compare":
                print("compare")
                n_of_bins=np.sqrt(length) + bin_length_index * (length - np.sqrt(length)) / density
                return int(n_of_bins)    

    @abstractmethod
    def get_histogram(self):
        pass

class Histogram_in_scales(Histogram):
    def __init__(self,_bins_in_scales,histogram_in_scales):
        self._hist_in_scales=histogram_in_scales
        self._bins_in_scales=_bins_in_scales
    
    def get_histogram(self,reverse=False):
        if reverse == False:
                return (self._bins_in_scales,self._hist_in_scales)
        if reverse == True:
                return (self._hist_in_scales,self._bins_in_scales)
    
