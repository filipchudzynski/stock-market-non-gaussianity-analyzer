import numpy as np
from CastaingEquationFitter import FitInScales
from abc import ABCMeta, abstractmethod

class Series(metaclass = ABCMeta):
    def __init__(self,series:np.array):
        self._series = series

    def get_series(self):
        return self._series

    def length(self):
        return len(self._series)
    
    def plot(self):
        plt.plot(self._series,linewidth=0.5)
        plt.show()

class SeriesDetrendedInScales(Series):
    def __init__(self,series,series_in_scales:dict=None):
        super().__init__(series)
        self._series_in_scales = series_in_scales

    def get_series(self):
        return super().get_series()

    def get_detrended_series_in_scales(self) : 
        if self._series_in_scales != None:
            return self._series_in_scales
        else:
            print("first detrend series in scale")

    def get_histograms_in_scales(self):
        if self._histogram_in_scales != None:
            return self._histogram_in_scales
        else:
            print("first calculate histogram")
    
    def get_fits_in_scales(self):
        if self._fits_in_scales != None:
            
            return self._fits_in_scales
        else:
            print("first calculate fit")

    def set_series_in_scales(self,series_in_scales):
        self._series_in_scales = series_in_scales

    def set_histogram_in_scales(self,histogram_in_scales):
        self._histogram_in_scales = histogram_in_scales

    def set_fits_in_scales(self,fits_in_scales):
        self._fits_in_scales = fits_in_scales
        #     print("plotting 10 first scales")
        #     cols,rows = 5,2
        #     fig,ax = plt.subplots(5,2)
        #     ax=ax.flatten()
        #     series_in_scale = list(zip(self._series_in_scales,
        #                             self._series_in_scales.values()))
        #     plt.get_current_fig_manager().full_screen_toggle() 
        #     plt.tight_layout()
        #     for i,data in enumerate(series_in_scale[:cols*row]):
        #         print(i)
        #         ax[i].plot(data[1])
        #         ax[i].set_title(str(data[0])+" - th scale")
        #     plt.show()    


