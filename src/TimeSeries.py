
from abc import ABCMeta, abstractmethod
from datetime import datetime

from Histogram import Histogram,Histogram_in_scales
from Series import Series, SeriesDetrendedInScales
from CastaingEquationFitter import CastaingEquationFitter, FitInScales
from CachedResultsLoader import legacy_load_lambdas


import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

from multiprocessing import Pool
import multiprocessing as mp

from sklearn.linear_model import LinearRegression

fontsize_for_article = 20
def get_equidistant_points_in_log_scales(start,stop,n_of_points):
        scales = 10**np.linspace(np.log10(start),np.log10(stop),n_of_points)
        scales = scales.astype(int)
        # print(scales)
        return scales

def change_dict_into_list_of_dictionaries(dict,keys,n_of_batches):
    list_of_dicts = []
    for b in range(n_of_batches):
        dict_in_batch = {}
        for k in keys:
            dict_in_batch[k] = dict[(b,k)]        
        list_of_dicts.append(dict_in_batch)
    
    return list_of_dicts


class Timeseries:
    class TimeseriesInBatches:
        def __init__(self,time_in_batches,series_in_batches,batch_range:tuple):
            if time_in_batches == None and series_in_batches == None:
                print("constructed empty TimeseriesInBatches object")
                return


            self._series_in_batches = series_in_batches[batch_range[0]:batch_range[1]]
            self._time_in_batches = time_in_batches[batch_range[0]:batch_range[1]]
            #redundancy
            #There is an object version and primitive version,
            #but should allow to use consistently the timeseries object
            #i.e Timeseries is divided in batches:
            #every batch has representation as a series and as a detrended series
            object_time_in_batches = []
            object_series_in_batches = []

            for time in self._time_in_batches:
                object_time_in_batches.append(Series(time))
            
            self._time_in_batches_object = object_time_in_batches
            
            for series in self._series_in_batches:
                object_series_in_batches.append(SeriesDetrendedInScales(series))
            
            self._series_in_batches_object = object_series_in_batches

            self._batch_range = batch_range

        def primitive_batches(self):
            return (self._time_in_batches, 
                    self._series_in_batches)

        def batches(self):
            return (self._time_in_batches_object, 
                    self._series_in_batches_object)

        def update_with_detrended_series(self,detrended_series_in_batches):
            for batches in zip(detrended_series_in_batches,
                                self._series_in_batches_object):
                batches[1].set_series_in_scales(batches[0])

        def update_with_histograms(self,histogram_in_batches):
            for batches in zip(histogram_in_batches,
                                self._series_in_batches_object):
                batches[1].set_histogram_in_scales(batches[0])

        def update_with_fit(self,fits_in_batches,params_in_batches):
            for batches in zip(fits_in_batches,
                                params_in_batches,
                                self._series_in_batches_object):
                            #fit_data,params,args
                bins=batches[2].get_histograms_in_scales().get_histogram()[0]
                fit_obj = FitInScales(batches[1],batches[0],
                            bins)
                batches[2].set_fits_in_scales( fit_obj)

        # def load_default(self,filename):
        #     hist = np.load("histogram_"+filename,allow_pickle=True)
        #     bins = np.load("bins_"+filename,allow_pickle=True)

        #     for batches in zip(bins,hist,
        #                         self._series_in_batches_object):
        #         hist_obj = Histogram_in_scales(batches[0],batches[1])
        #         batches[2].set_histogram_in_scales(hist_obj)
            
        #     fits = np.load("fits"+filename,allow_pickle=True)
        #     params = np.load("params"+filename,allow_pickle=True)
        #     for batches in zip(fits,params,
        #                         self._series_in_batches_object):
        #         fit_obj = FitInScales(batches[1][1],batches[1][0],
        #                             batches[0][0])
        #         batches[2].set_fits_in_scales(fit_obj)
        
        def load_cached_data(self,filename_hist, filename_bins,
                        filename_fit, filename_params,legacy=True,
                        params_alternative_filename:str=None):
            hist = np.load(filename_hist,allow_pickle=True)
            bins = np.load(filename_bins,allow_pickle=True)
            fits = np.load(filename_fit,allow_pickle=True)
            #create empty placeholder for cached data
            object_series_in_batches = []
            for batches in range(len(hist)):
                object_series_in_batches.append(SeriesDetrendedInScales(np.array([])))
            self._series_in_batches_object = object_series_in_batches

            if legacy is True:
                params = legacy_load_lambdas(filename=filename_params,legacy_txt_filename=params_alternative_filename)
                fits = change_dict_into_list_of_dictionaries(fits.item(),params[0].keys(),len(params))
                # np.save(filename_fit,fits)
            else:
                params = np.load(filename_params,allow_pickle=True)
                example_key = list(fits[0].keys())[0]
                if len(fits[0][example_key]) == 1:
                    for b_ind,batch in enumerate(fits):
                        for scale in batch:
                            fits[b_ind][scale] = fits[b_ind][scale][0] 
    

            #get_rid_of_empty_bins_in_legacy_histograms
            if legacy is True:
                non_zero_hist=[]
                non_zero_bins=[]
                non_zero_fits=[]
                for batch in zip(bins,hist,fits):
                    hist_dict={}
                    bins_dict={}
                    fits_dict={}
                    for key in batch[0]:
                        bins_dict[key] = batch[0][key][:-1][batch[1][key]!=0]
                        hist_dict[key] = batch[1][key][batch[1][key]!=0]
                        fits_dict[key] = np.array(batch[2][key])[batch[1][key]!=0]
                    non_zero_bins.append(bins_dict)
                    non_zero_hist.append(hist_dict)
                    non_zero_fits.append(fits_dict)

                bins = non_zero_bins
                hist = non_zero_hist
                fits = non_zero_fits

            for batches in zip(bins,hist,
                                self._series_in_batches_object):
                hist_obj = Histogram_in_scales(batches[0],batches[1])
                batches[2].set_histogram_in_scales(hist_obj)

            
            
            

            # print(fits)
            # print(len(params),len(self._series_in_batches_object))
            for batches in zip(fits,params,bins,
                                self._series_in_batches_object):
                fit_obj = FitInScales(batches[1],
                                    batches[0],batches[2])
                batches[3].set_fits_in_scales(fit_obj)
        

        ##PLOT METHODS
        ##################################################################

        def plot_all_series(self,single_plot=None):
            print("plotting 8 first batches")
            cols,rows = 4,2
            fig,ax = plt.subplots(4,2)
            ax=ax.flatten()
            time_batches, series_batches = self.primitive_batches()
            batches = list(zip(time_batches,series_batches))
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            for i,data in enumerate(batches[:cols*rows]):
                ax[i].plot(data[0],data[1])
                ax[i].set_title(str(i)+" - th batch")
            plt.show()

        def plot_detrended_series_for_single_batch(self,batch_ind=0):
            print("plotting 10 first scales")
            rows,cols = 5,2
            fig,ax = plt.subplots(rows,cols)
            ax=ax.flatten()
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            time = self._time_in_batches_object[batch_ind].get_series()
            print(time[0] is np.dtype('float64'),type(time[0]))
            if type(time[0]) == np.dtype('float64'):
                print("float")
                plt.suptitle("BATCH " + str(batch_ind + 1)
                                +"({:.1f}-{:.1f})".format(time[0],time[-1]),
                                fontsize=16)
            else:
                print("str")
                plt.suptitle("BATCH " + str(batch_ind + 1)
                                +"({}-{})".format(time[0],time[-1]),fontsize=16) 
            batch_primitive = series_batches[batch_ind].get_detrended_series_in_scales()
            keys_list = list(batch_primitive.keys())[:rows*cols]
            for scaleIndex, scale in enumerate(keys_list):
                ax[scaleIndex].plot(batch_primitive[scale],label=str(scale)+" - scale")
                ax[scaleIndex].legend()
            plt.show()
        
        #TODO plot all detrended series

        def plot_histograms_for_single_batch(self,batch_ind=0):
            print("plotting 10 first histogram")
            rows,cols = 5,2
            fig,ax = plt.subplots(rows,cols)
            ax=ax.flatten()
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            bins_primitive,hist_primitive = \
                        series_batches[batch_ind].get_histograms_in_scales().get_histogram()
            keys_list = list(bins_primitive.keys())[:rows*cols]
            for scaleIndex, scale in enumerate(keys_list):
                #TODO where to get rid of this one point
                ax[scaleIndex].scatter(bins_primitive[scale][:-1],
                            hist_primitive[scale], 
                            label=str(scale)+" - scale")
                ax[scaleIndex].legend()
                ax[scaleIndex].set_yscale('log')
                ax[scaleIndex].set_xlim(-15,15)
                ax[scaleIndex].set_ylim(bottom=1)
            plt.show()

        def plot_cumulative_histograms(self):
            print("plotting 6 first batches")
            rows,cols = 2,2
            fig,ax = plt.subplots(rows,cols)
            ax=ax.flatten()
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            n_of_hist=\
                len(list(series_batches[0].get_histograms_in_scales().get_histogram()[0].keys()))

            scaling_factor=get_equidistant_points_in_log_scales(8,10**(n_of_hist-2),n_of_hist)
            for batchIndex,batch in enumerate(series_batches[:rows*cols]):
                bins_primitive,hist_primitive = \
                            batch.get_histograms_in_scales().get_histogram()
                for scaleIndex, scale in enumerate(bins_primitive.keys()):
                    #TODO where to get rid of this one point

                    # translated_hist = (hist_non_zero/((scale)))
                    # plt.scatter(bins_non_zero,translated_hist)
                    # plt.yscale('log')

                    # plt.xlim(-15,15)
                    # # plt.show()
                    if len(bins_primitive[scale]) == len(hist_primitive[scale]):
                        ax[batchIndex].scatter(bins_primitive[scale],
                                hist_primitive[scale]/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale",s=1)
                    else:
                        ax[batchIndex].scatter(bins_primitive[scale][:-1],
                                hist_primitive[scale]/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale",s=1)
                    ax[batchIndex].legend()
                    ax[batchIndex].set_yscale('log')
                    ax[batchIndex].set_xlim(-15,15)
                    ax[batchIndex].set_ylim(10**(-n_of_hist+1),100)
            plt.show()

        def plot_fits_single_batch(self,batch_ind=None,half=False,confidence_bands=False):
            series_batches = self._series_in_batches_object
            if batch_ind is None:
                batch_ind = int(np.random.rand()*len(series_batches))    
            print("plotting batch "+ str(batch_ind))
            
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            n_of_hist=\
                len(list(series_batches[0].get_histograms_in_scales().get_histogram()[0].keys()))
            print(n_of_hist)
            scaling_factor=get_equidistant_points_in_log_scales(8,10**(n_of_hist-2),n_of_hist)
            batch = series_batches[batch_ind]
            _,fits = batch.get_fits_in_scales().get_fit()
            bins,hist = batch.get_histograms_in_scales().get_histogram()
            if half == True:
                print("plotting in two halfes")
                divided_keys = [sorted(hist.keys())[:10],sorted(hist.keys())[10:]]
                for keys in divided_keys:
                    for scaleIndex, scale in enumerate(keys):
                        #TODO where to get rid of this one point
                        if len(bins[scale]) == len(hist[scale]):
                            if confidence_bands==False:
                                plt.plot(bins[scale],
                                    np.array(fits[scale])/scaling_factor[scaleIndex], 
                                    label=str(scale)+" - scale")
                            else:
                                plt.plot(bins[scale],
                                    np.array(fits[scale][0])/scaling_factor[scaleIndex], 
                                    label=str(scale)+" - scale")
                            plt.scatter(bins[scale],hist[scale]/scaling_factor[scaleIndex],s=1,marker="s",alpha=0.1)        
                        else:
                            plt.plot(bins[scale][:-1],
                                    np.array(fits[scale])/scaling_factor[scaleIndex], 
                                    label=str(scale)+" - scale")
                            plt.scatter(bins[scale][:-1],hist[scale]/scaling_factor[scaleIndex],s=1,marker="s",alpha=0.1)        
                    
                        # plt.legend()
                        plt.yscale('log')
                        plt.xlim(-15,15)
                        plt.ylim(10**(-21),10**3)
                        plt.xticks(fontsize=12)
                        plt.yticks(fontsize=12)
                        plt.ylabel(r'$P_s(\Delta_sZ)$')
                        plt.xlabel(r'$\Delta_sZ/\sigma_s$')
                    plt.show()
            else:
                print("plotting every second scale")
                for scaleIndex, scale in enumerate(sorted(hist.keys())[1::2]):
                    #TODO where to get rid of this one point
                    if len(bins[scale]) == len(hist[scale]):
                        if confidence_bands==False:
                            plt.plot(bins[scale],
                                np.array(fits[scale])/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale")
                        else:
                            plt.plot(bins[scale],
                                np.array(fits[scale][0])/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale")
                        plt.scatter(bins[scale],hist[scale]/scaling_factor[scaleIndex],s=1,marker="s",alpha=0.1)        
                    else:
                        if confidence_bands==False:
                            plt.plot(bins[scale],
                                np.array(fits[scale])/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale")
                        else:
                            plt.plot(bins[scale],
                                np.array(fits[scale][0])/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale")
                        plt.scatter(bins[scale][:-1],hist[scale]/scaling_factor[scaleIndex],s=1,marker="s",alpha=0.1)        
                
                    # plt.legend()
                    plt.yscale('log')
                    plt.xlim(-15,15)
                    plt.ylim(10**(-21),10**3)
                    plt.xticks(fontsize=12)
                    plt.yticks(fontsize=12)
                    plt.ylabel(r'$P_s(\Delta_sZ)$',fontsize=12)
                    plt.xlabel(r'$\Delta_sZ/\sigma_s$',fontsize=12)
                plt.show()

        def plot_fits(self):
            print("plotting 6 first batches")
            rows,cols = 2,2
            fig,ax = plt.subplots(rows,cols)
            ax=ax.flatten()
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            n_of_hist=\
                len(list(series_batches[0].get_histograms_in_scales().get_histogram()[0].keys()))
            print(n_of_hist)
            scaling_factor=get_equidistant_points_in_log_scales(8,10**(n_of_hist-2),n_of_hist)
            for batchIndex,batch in enumerate(series_batches[:rows*cols]):
                _,fits=batch.get_fits_in_scales().get_fit()
                print(len(fits),fits[1][7])
                bins,hist = batch.get_histograms_in_scales().get_histogram()
                for scaleIndex, scale in enumerate(sorted(hist.keys())):
                    #TODO where to get rid of this one point
                    if len(bins[scale]) == len(hist[scale]):
                        # print(scale,fits[0][scale])
                        ax[batchIndex].plot(bins[scale],
                        fits[0][scale]/scaling_factor[scaleIndex],
                                label=str(scale)+" - scale")
                        ax[batchIndex].scatter(bins[scale],hist[scale]/scaling_factor[scaleIndex],s=1)        
                    else:
                        ax[batchIndex].plot(bins[scale][:-1],
                                fits[scale]/scaling_factor[scaleIndex], 
                                label=str(scale)+" - scale")
                        ax[batchIndex].scatter(bins[scale][:-1],hist[scale]/scaling_factor[scaleIndex],s=1)        

                    ax[batchIndex].legend()
                    ax[batchIndex].set_yscale('log')
                    ax[batchIndex].set_xlim(-15,15)
                    ax[batchIndex].set_ylim(10**(-n_of_hist+1),100)
            plt.show()

        def plot_lambda_in_scales(self,cumulative=True,scatter=True,zoomed_limit=None):
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            if cumulative == False:
                print("plotting 6 first batches")
                rows,cols = 3,2
                fig,ax = plt.subplots(rows,cols)
                ax=ax.flatten()
                for batchIndex,batch in enumerate(series_batches[:rows*cols]):
                    params = batch.get_fits_in_scales().get_params()
                    lambdas = [params[p][0] for p in params]
                    ax[batchIndex].scatter(params.keys(),lambdas**2,label=str(batchIndex))
                    ax[batchIndex].legend()
                    ax[batchIndex].set_xscale('log')
            else:
                for batchIndex,batch in enumerate(series_batches):
                    params = batch.get_fits_in_scales().get_params()
                    lambdas = [params[p][0] for p in params]
                    if scatter==True:
                        plt.scatter(params.keys(),lambdas**2)
                    else:
                        keys,lambdas = zip(*sorted(zip(params.keys(),lambdas)))
                        plt.plot(keys,np.array(lambdas)**2,label=str(batchIndex))
                        # plt.legend()
                        # plt.ylim(0,1)
                    plt.xscale('log')    
            plt.xticks(fontsize=12)
            plt.yticks(fontsize=12)
            plt.ylabel(r'$\lambda^2$',fontsize=12)
            plt.xlabel('s [min]',fontsize=12)
            plt.show()
            if zoomed_limit is not None:
                for batchIndex,batch in enumerate(series_batches):
                    params = batch.get_fits_in_scales().get_params()
                    lambdas = [params[p][0] for p in params]
                    keys,lambdas = zip(*sorted(zip(params.keys(),lambdas)))
                    plt.plot(keys,lambdas**2,label=str(batchIndex))
                    plt.xscale('log') 
                    plt.xlim(8,zoomed_limit)
                plt.ylabel(r'$\lambda^2$',fontsize=12)
                plt.xlabel('s [min]',fontsize=12)   
                plt.xticks(fontsize=12)
                plt.yticks(fontsize=12)
                plt.show()
        #TODO make a getKeys convenience method
        def plot_lambdas_averaged_over_batches(self,superimpose=False,error_bar=True):
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            minMaxAvgDict= {}
            for k in series_batches[0].get_fits_in_scales().get_params().keys():
                minMaxAvgDict[k]=[0,0,[]]
        
            for batch in series_batches:
                params = batch.get_fits_in_scales().get_params()
                lambdas = [params[p][0] for p in params]
                keys,lambdas = zip(*sorted(zip(params.keys(),lambdas)))
                if superimpose == True:
                    plt.plot(keys,np.array(lambdas)**2)
                #lambdas in given batch
                for p in params:
                    minMaxAvgDict[p][2].append(params[p][0]**2)
                    
                #calculate mean and std of lambda for given scale
                for p in params:
                    minMaxAvgDict[p][0] = np.mean(minMaxAvgDict[p][2])
                    minMaxAvgDict[p][1] = np.std(minMaxAvgDict[p][2])

            #TODO fix that, not clear
            xList, yList, errorList = zip(*sorted(zip(minMaxAvgDict.keys(),
                                    np.array(list(minMaxAvgDict.values()))[:,0],
                                    np.array(list(minMaxAvgDict.values()))[:,1])))
            # plt.errorbar(xList,yList,yerr=errorList,fmt='o')
            
            if error_bar == True:
                plt.errorbar(xList,yList,yerr=errorList)
            else:
                plt.plot(xList,yList,color='black', linewidth=2, dashes=[3, 3])
                plt.fill_between(xList,np.array(yList)-np.array(errorList),np.array(yList)+np.array(errorList),alpha=0.2)
            plt.xscale('log')
            # plt.yscale('log')
            # plt.title('scale dependence of $\lambda^2$')
            plt.ylabel('$\lambda^2$',fontsize=12)
            plt.xticks(fontsize=12)
            plt.yticks(fontsize=12)
            plt.xlabel('s [min]',fontsize=12)
            plt.show()

            # plt.errorbar(xList,yList,yerr=errorList,fmt='o')
            plt.errorbar(xList,yList,yerr=errorList)
            plt.xscale('log')
            plt.yscale('log')
            # plt.title('scale dependence of log($\lambda^2$)')
            plt.xticks(fontsize=12)
            plt.yticks(fontsize=12)
            plt.ylabel('log($\lambda^2$)',fontsize=12)
            plt.xlabel('log(s)',fontsize=12)
            plt.show()

        def plot_lambas_heatmap(self):
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            lambdas_array = []
            for batchIndex,batch in enumerate(series_batches):
                params = batch.get_fits_in_scales().get_params()
                lambdas = [params[p][0] for p in params]
                keys,lambdas = zip(*sorted(zip(params.keys(),np.array(lambdas)**2)))
                lambdas_array.append(lambdas)

            keys = [ str(k) for k in keys]
            plt.imshow(np.transpose(lambdas_array),vmax=0.9)
            plt.colorbar()
            plt.xlabel('batch id',fontsize=12)
            plt.ylabel('scale',fontsize=12)
            plt.xticks(fontsize=12)
            plt.yticks(np.arange(len(keys)),keys,fontsize=12)
            ax=plt.gca()
            ax.set_yticks(ax.get_yticks()[::2])
            plt.show()
        
        def plot_lambdas_surface(self):
            series_batches = self._series_in_batches_object
            plt.get_current_fig_manager().full_screen_toggle() 
            plt.tight_layout()
            lambdas_array = []
            for batchIndex,batch in enumerate(series_batches):
                params = batch.get_fits_in_scales().get_params()
                lambdas = [params[p][0] for p in params]
                keys,lambdas = zip(*sorted(zip(params.keys(),lambdas**2)))
                lambdas_array.append(lambdas)

            # keys = [ str(k) for k in keys]
            X, Y = np.meshgrid(keys, np.arange(len(series_batches)))
            fig = plt.figure()
            Z = np.array(lambdas_array)
            # print(X)
            # print(Y)
            # print(Z)
            ax = fig.add_subplot(111, projection='3d')
            ax.view_init(30, 10)
            surf = ax.plot_surface(X, Y, Z,cmap=cm.get_cmap("autumn"))
            fig.colorbar(surf, shrink=0.5, aspect=5)
            plt.show()

    ##############################################
    # end of TimeseriesInBatches    
 
    def __init__(self,series=None,time=None,name=None):
        self._series = series
        self._time = time
        self._name = name
        self._timeseries_in_batches = None
    
    def load_data(self,loader):
        time,series, self._name, volume = loader.load()
        self._series = Series(series)
        self._time = Series(time)
        if volume is None:
            print("volume data not available")
            self._volume = None
        else:
            self._volume = Series(volume)

    def get_timeseries(self):
        return self._time, self._series 

    def plot(self,linewidth=0.5,fontsize=20,batches_limits=False,volume=False,log=False):
        time, series = self._time.get_series(), self._series.get_series()     
        if volume == False:
            plt.plot(time,series,linewidth=linewidth)
            # plt.title(self._name,fontsize=fontsize)
            if log==True:
                plt.yscale('log')
            if batches_limits == True:
                time_batches,_ = self._timeseries_in_batches.primitive_batches()
                y=np.max(series)*0.995
                for batch_ind, time_batch in enumerate(time_batches):
                    plt.axvline(x=time_batch[-1])
                    mid_point = time_batch[int(len(time_batch)/4)]
                    plt.text(mid_point,y,str(batch_ind))
                plt.xticks(rotation=50)
                ax=plt.gca()
                ax.set_xticks(np.arange(1985,1997))
            plt.ylabel(r'$S&P$'+ " index")
            plt.xlabel(r'time [years]')
            plt.show()
        if self._volume != None and volume==True:
            volume = self._volume.get_series() 
            plt.plot(time,volume,linewidth=linewidth)
            plt.title(self._name+" Volume",fontsize=fontsize)
            if batches_limits == True:
                time_batches,_ = self._timeseries_in_batches.primitive_batches()
                y=np.max(volume) + 100
                for batch_ind, time_batch in enumerate(time_batches):
                    plt.axvline(x=time_batch[-1])
                    mid_point = time_batch[int(len(time_batch)/4)]
                    plt.text(mid_point,y,str(batch_ind))
            plt.show()

    def plot_batches(self):
        self._timeseries_in_batches.plot_all_series()

    def plot_histograms(self):
        self._timeseries_in_batches.plot_cumulative_histograms()

    def plot_fits(self):
        self._timeseries_in_batches.plot_fits()
    
    def plot_fits_single_batch(self,batch_ind=None,half=False,confidence_bands=False):
        self._timeseries_in_batches.plot_fits_single_batch(batch_ind,half,confidence_bands)

    def plot_lambdas(self,cumulative=False,scatter=True,zoomed_limit=None):
        self._timeseries_in_batches.plot_lambda_in_scales(cumulative,scatter,zoomed_limit)

    def plot_lambdas_averaged_over_batches(self,superimpose=False,error_bar=True):
        self._timeseries_in_batches.plot_lambdas_averaged_over_batches(superimpose,error_bar)
    
    def plot_lambdas_heatmap(self):
        self._timeseries_in_batches.plot_lambas_heatmap()

    def plot_lambdas_surface(self):
        self._timeseries_in_batches.plot_lambdas_surface()

    #methods related to batches
    ######################################################
    def divide_into_batches(self,n_of_batches,batch_range=None):

        if batch_range == None:
                    batch_range=(0,n_of_batches)               
        length=self._series.length()
        batch_length = int(length/n_of_batches)
        batch_limits = [x*batch_length for x in range(n_of_batches+1)]
        
        series_batches = []
        time_batches = []

        series = self._series.get_series()
        time = self._time.get_series()
        for i in range(n_of_batches):
                series_batches.append(
                    series[batch_limits[i]:batch_limits[i+1]]
                )
                time_batches.append(
                    time[batch_limits[i]:batch_limits[i+1]]
                )
        # print("batch length:",len(series_batches[1]))
        self._timeseries_in_batches = self.TimeseriesInBatches(time_batches,
                                                                    series_batches,
                                                                     batch_range)
        self._batches_limits = batch_limits
            
    def timeseries_in_batches(self):
        return self._timeseries_in_batches

    def load_cached_data(self,filenames:dict,legacy=True,params_alternative_filename:str=None):
        if self._timeseries_in_batches is None:
            batches_object = self.TimeseriesInBatches(None,None,None)
        else:
            batches_object = self._timeseries_in_batches
        batches_object.load_cached_data(filenames["histogram"],filenames["bins"],filenames["fit"],
                            filenames["params"],legacy,params_alternative_filename)
        self._timeseries_in_batches=batches_object

    #methods related to detrending
    #######################################################
    
    #detrending in single scale - window_size
    def _detrending_in_windows(self,window_size,time,series):
        s = window_size
        n_of_windows = int(len(series)/(2*s))
        intervals = [(1 + s * (k - 1),s * (k + 1)) for k in range(1,n_of_windows,2)]
        # print(intervals)
        #transform Y to column vectors for the fitting proceudre
        T = time
        Y = series.reshape(-1, 1)

        xStar = []
        for a,b in intervals:
            model = LinearRegression()
            model.fit(T[a:b],Y[a:b])
            xStar+=list(np.transpose(Y[a:b] - (model.intercept_ + model.coef_ * T[a:b]))[0])

        #detrended log returns
        xStar = np.array(xStar)
        deltaZ = xStar[s:]-xStar[:-s]
        
        return np.array(deltaZ, dtype=np.float64)


    def detrended_log_returns(self,scales_description,sequential=False):
        scales = get_equidistant_points_in_log_scales(
                                                    scales_description['start'],
                                                    scales_description['end'],
                                                    scales_description['N'])
        deltaZ = []
        #start = timer()
        time_batches, series_batches = self._timeseries_in_batches.primitive_batches()
        # print(series_batches)
        for b in series_batches: 
                T = np.array(range(len(b)))
                T = T.reshape(-1,1)
                deltaZInBatch = {}
                
                logb=np.log(b)
                
                #sequential calculation
                if sequential == True:
                        for i in scales:
                            deltaZInBatch[i] = self._detrending_in_windows(i,T,logb)
                        deltaZ.append(deltaZInBatch)
                

                #parallel
                else:        
                        args = [(i,T,logb) for i in scales]
                        p = Pool()
                        deltaZInBatch = p.starmap(self._detrending_in_windows,args)
                        deltaZInBatch = dict(zip(scales,deltaZInBatch))
                        deltaZ.append(deltaZInBatch)
                        p.terminate()

        #end = timer()
        #print(end - start) # Time in seconds, e.g. 5.38091952400282
        self._timeseries_in_batches.update_with_detrended_series(deltaZ)
    
    # METHODS RELATED TO HISTOGRAM
    #####################################################################
    def calculate_histogram(self,bins_fname="test_bins",
                hist_fname="test_hist", method = "my_initial",
                separate_plots_y_axis = True, enable_plot = True,bin_length_index=0):
        
        #histogram
        hdict = []
        #bins
        bins = []
        #list of histogram objects
        histograms=[]
        time_batches,series_batches = self._timeseries_in_batches.batches()
        print(len(series_batches))
        for batchIndex,b in enumerate(series_batches):
                series_in_scales=b.get_detrended_series_in_scales()
                series_in_scales.keys()
                hdictInScales={}
                binsInScales={}
                for scaleIndex,scale in enumerate(series_in_scales.keys()):
                        std = np.std(series_in_scales[scale])
                        #with standarization!
                        standarized_series = series_in_scales[scale]/std
                        hdictInScales[scale], binsInScales[scale] = np.histogram(standarized_series,
                                bins=Histogram.calculate_number_of_bins(method,len(series_in_scales[scale]),
                                scaleIndex,std,bin_length_index))
                        
                        
                        #remove bins without scores
                        binsInScales[scale] = binsInScales[scale][:-1]
                        hdictInScales[scale] = hdictInScales[scale]
              
                        
                hdict.append(hdictInScales)
                bins.append(binsInScales)
                histograms.append(Histogram_in_scales(binsInScales,hdictInScales))
        np.save(bins_fname,bins)
        np.save(hist_fname,hdict)
        self._timeseries_in_batches.update_with_histograms(histograms)

    def prepare_args(self):
        args = []
        batches = self._timeseries_in_batches._series_in_batches_object
        scaleArray = batches[0].get_histograms_in_scales().get_histogram()[0].keys()
        mapping_args = []
        # print(mapping_args)
        for batch in batches:
            hist_tuple = batch.get_histograms_in_scales().get_histogram(reverse=True)        
            args += [ (hist_tuple[0][scale],hist_tuple[1][scale])
                                                        for scale in scaleArray]
            mapping_args += [scaleArray]                                      
        return args, mapping_args                                                

    def fit_castaing_equation(self,params_filename,
                    fit_filename,reports_filename,parallel=True,confidence=False):

        ##jeszcze zerowy
        print("started: ",str(datetime.now()))
        # for batchIndex,b in enumerate(batches):
        #if batchIndex >= lastBatch:
        # print("batch index",batchIndex)
        results_list = []
        params_list = []
        args,mapping_args = self.prepare_args()
        
        if parallel == True:
                p = Pool()
                #params_list = p.starmap(fit_data,args)
                if confidence == False:
                        results_list = p.starmap(CastaingEquationFitter.fit_data,args)
                else:
                        results_list = p.starmap(CastaingEquationFitter.fit_data_with_conf_interval,args)
                
        else:
                if confidence == False:
                        for a in args:
                                results_list.append(CastaingEquationFitter.fit_data(a[0],a[1]) )
                else:
                        for a in args:
                                results_list.append(CastaingEquationFitter.fit_data_with_conf_interval(a[0],a[1]) )

        print("ended: ",str(datetime.now()))
        #TODO serious error?
        params_list = [results[:2] for results in results_list]
        print(params_list)
        print(args)
        params = []
        for batch_ind,batch in enumerate(mapping_args):
            params.append({})
            for scale_ind, scale in enumerate(batch):
                #list for clarity of what is happening in this loop
                    params[batch_ind][scale]=params_list[batch_ind*len(batch)+scale_ind]

        print(params)


        fit_report_list = [results[2] for results in results_list]

        reports = []
        for batch_ind,batch in enumerate(mapping_args):
            reports.append({})
            for scale_ind, scale in enumerate(batch):
                #list for clarity of what is happening in this loop
                    reports[batch_ind][scale]=fit_report_list[batch_ind*len(batch)+scale_ind]

        print(reports)
        # params_dict = dict(zip(args,params_list))
                # fitResult = multiplicativeModel.fit(hdict[batchIndex][i],x=bins[batchIndex][i][:-1],A=2000,params=1.5)
                # print(fitResult.best_values)
                # paramsDict[i] = fitResult.best_values['lambdas']
                # plt.plot(bins[batchIndex][i][:-1], fitResult.best_fit)
                # plt.scatter(bins[batchIndex][i][:-1],hdict[batchIndex][i],label=str(i))
                # plt.yscale('log')
                # plt.xlim(-15,15)
                # plt.ylim(10**(-14),10**5)
                # plt.legend()
        time_stamp='D_{0:%d}-{0:%m}-{0:%Y}_T_{0:%IH%MM%SS}.'.format(datetime.now())
        FILE = open(params_filename + time_stamp+".txt",'a')
        FILE.write(str(params)+"\n")
        FILE.close()
        # params = load_params(params_dict = params_dict)
        # fit_args = [(bins[batch_index][i][:-1], params[batch_index][i][1],
        #                 params[batch_index][i][0]) for batch_index,i in mapping_args]
        fit_list = [results[3:] for results in results_list]#p.starmap(Ps,fit_args)
        print(fit_list)
        fits = []
        for batch_ind,batch in enumerate(mapping_args):
            fits.append({})
            for scale_ind, scale in enumerate(batch):
                if confidence is True:
                    fits[batch_ind][scale]=(fit_list[batch_ind*len(batch)+scale_ind][0],
                                            fit_list[batch_ind*len(batch)+scale_ind][1])
                else:
                    fits[batch_ind][scale]=fit_list[batch_ind*len(batch)+scale_ind][0]
        
        # np.save(fit_filename+"legacy",fit_dict)
        # np.save(params_filename+"legacy",params_dict)
        p.terminate()

        #transforming dictionary with tuple keys (batch,scale)
        #into list of batches [{scale: ...},...]

        # scaleArray = batches[0].get_histograms_in_scales().get_histogram()[0].keys()
        # fits = change_dict_into_list_of_dictionaries(fit_dict,scaleArray,len(batches))
        # params = change_dict_into_list_of_dictionaries(params_dict,scaleArray,len(batches))
        print(fits)
        print(params)
        self._timeseries_in_batches.update_with_fit(fits,params)
        np.save(fit_filename,fits)
        np.save(params_filename,params)
        np.save(reports_filename,reports)

