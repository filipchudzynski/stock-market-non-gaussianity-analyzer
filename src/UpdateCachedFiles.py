from CachedResultsLoader import legacy_load_lambdas
from TimeSeries import change_dict_into_list_of_dictionaries
import glob
import numpy as np

abs_path = "/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/"
cached_data_filenames = glob.glob("cached_numpy_data_for_plots/recent/params*.npy")
for ind,filename in enumerate(cached_data_filenames):
     print(ind,filename,ind)
     fits = np.load(filename,allow_pickle=True)
     print(fits)
#     if ind == 16:
#         # fits = fits.item()
#         print(type(fits))
#         print(fits.keys())
#         batches = set([key[0] for key in fits.keys()])
#         keys = set([key[1] for key in fits.keys()])
#         fits = change_dict_into_list_of_dictionaries(fits,keys,len(batches))
#         print(type(fits))
#         print(type(fits[0]))

# # x = np.load("test_np_save.npy",allow_pickle=True)
# # print(x)
        # np.save(filename,fits)
# params_filenames = glob.glob("lambda_results/fit_results*.txt")
# for ind,filename in enumerate(params_filenames):
#     print(ind,filename[:-30],ind)
#     filename_params="/media/filip/3EE0999CE0995B4B/FUW_projects_programming/complexity_and_emergence/non_gaussian_distribution/"+\
#     "cached_numpy_data_for_plots/recent/"+"params" + filename[len("lambda_results/fit_results"):-30]+".npy"
# # fits = np.load(filename_fit,allow_pickle=True)

#     params = legacy_load_lambdas(filename=filename_params,legacy_txt_filename=filename)
# fits = change_dict_into_list_of_dictionaries(fits.item(),params[0].keys(),len(params))
# np.save(filename_fit,fits)