#Example usage of the library
#This example is using pre-cached data to reduce time of calculation
from TimeSeries import Timeseries
from DataLoader import BitcoinDataLoader,SandPDataLoader
from datetime import datetime
from Config import Config
import os
cwd = os.getcwd()
print(cwd)

def SandP_non_gaussianity_with_respect_to_batch(batch_size,pts,jupyter=False):
    if jupyter is True:
        prefix = "../"
    else:
        prefix = ""
    params = {
        "calculation_type":"",
        "scales_range":{"start":8,"end":2**11,"N":pts},
        "dataset":"SandP",
        "n_of_batches":batch_size,
        "batch_range":None,
        "bin_calc_method":"modified",
        "dataset_folder":prefix+"datasets/",
        "dataset_filename":'SandP_log2min.dat',
        "cached_data_folder":prefix+"cached_numpy_data_for_plots/recent",
        "fontsize":15}
    config = Config(params=params,timeseries=Timeseries())
    config.load_timeseries()
    config.divide_into_batches()
    config.load_cached_data()
    return config.get_timeseries()
#In[1]:
##BITCOIN ANALYSIS
batches = {"year":13,"half":26,"quarter":52}
#year
BY = SandP_non_gaussianity_with_respect_to_batch(batches["year"],10)
#half
BH = SandP_non_gaussianity_with_respect_to_batch(batches["half"],20)
# # #quarter
BQ = SandP_non_gaussianity_with_respect_to_batch(batches["quarter"],20)


#In[2]:
BY.plot()

BY.plot_lambdas_averaged_over_batches()
BH.plot_lambdas_averaged_over_batches()
BQ.plot_lambdas_averaged_over_batches()
BQ.detrended_log_returns()
time, series = BQ.timeseries_in_batches().primitive_batches()